#!/usr/bin/env python
# encoding: utf8

#
#
# Copyright 2020 Multidevas SA

# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

from __future__ import unicode_literals

import requests
from ayurdevas.error import AyurdevasError
from ayurdevas import __version__

class Api(object):
    """A python interface into the Ayurdevas Ecommerce API"""

    def __init__(self, base_url, api_key):
        """Instantiate a new ayurdevas.Api object.

        Args:
          api_key (str):
            Your Ecommerce user's api_key.
          base_url (str):
            The base URL to use.
        """

        self.base_url = str(base_url)

        self._InitializeRequestHeaders()
        self._InitializeUserAgent()
        self._InitializeDefaultParameters()
        self._SetCredentials(api_key)

        # self.rate_limit = RateLimit()

    def _InitializeRequestHeaders(self):
        self._request_headers = {
            'accept': 'application/json',
            'accept': 'application/vnd.api.v1',
        }

    def _InitializeUserAgent(self):
        user_agent = 'python-ayurdevas/{0}'.format(__version__)
        self.SetUserAgent(user_agent)

    def _InitializeDefaultParameters(self):
        self._default_params = {}

    def _SetCredentials(self, api_key):
        self._request_headers['authorization'] = "Token token={0}".format(api_key)

    def GetPendingOrders(self, filters=None):
        """ Returns a list of pending orders ids. """
        url = '%s/purchases/' % self.base_url
        resp = self._RequestUrl(url, 'GET', filters)
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)
        return data

    def GetOrder(self, resid):
        """Returns a single order.

        Args:
            resid(int, str):
                The id we want to retrieve.

        Returns:
          A JSON representation of that order
        """

        url = '%s/purchases/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'GET')
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def UpdateOrder(self, resid, params):
        """ Updates record data

        Args:
            resid(int, str):
                The id we want to update.
            params(dict):
                The data to update

        Returns:

        """
        url = '%s/purchases/%s' % (self.base_url, resid)
        response = self._RequestUrl(url, 'PATCH', params)
        self._RaiseForHeaderStatus(response)
        data = self._ParseAndCheck(response)

        return data

    def GetCustomer(self, resid):
        """Returns a single partner.

        Args:
            resid(int, str):
                The id we want to retrieve.

        Returns:
          A JSON representation of that customer
        """

        url = '%s/users/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'GET')
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def CreateCustomer(self, params):
        url = '%s/users/' % (self.base_url)
        response = self._RequestUrl(url, 'POST', params)
        self._RaiseForHeaderStatus(response)
        data = self._ParseAndCheck(response)

        return data

    def UpdateCustomer(self, cusid, params):
        """ Updates record data

        Args:
            cusid(int, str):
                The id we want to update.
            params(dict):
                The data to update

        Returns:

        """
        url = '%s/users/%s' % (self.base_url, cusid)
        response = self._RequestUrl(url, 'PATCH', params)
        self._RaiseForHeaderStatus(response)
        data = self._ParseAndCheck(response)

        return data

    def GetProduct(self, resid):
        """Returns a single resource.

        Args:
            resid(int, str):
                The id we want to retrieve.

        Returns:
          A JSON representation of that resource
        """
        url = '%s/products/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'GET')
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def CreateProduct(self, data):
        url = '%s/products/' % (self.base_url)
        resp = self._RequestUrl(url, 'POST', data)
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def UpdateProduct(self, resid, data):
        """ Updates record data

        Args:
            resid(int, str):
                The id we want to update.
            data(dict):
                The data to update

        Returns:

        """
        url = '%s/products/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'PATCH', data)
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def GetCampaign(self, resid):
        """Returns a single resource.

        Args:
            resid(int, str):
                The id we want to retrieve.

        Returns:
          A JSON representation of that resource
        """
        url = '%s/campaigns/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'GET')
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def CreateCampaign(self, data):
        url = '%s/campaigns/' % (self.base_url)
        resp = self._RequestUrl(url, 'POST', data)
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def UpdateCampaign(self, resid, data):
        """ Updates record data

        Args:
            resid(int, str):
                The id we want to update.
            data(dict):
                The data to update

        Returns:

        """
        url = '%s/campaigns/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'PATCH', data)
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def GetPointsLine(self, resid):
        """Returns a single resource.

        Args:
            resid(int, str):
                The id we want to retrieve.

        Returns:
          A JSON representation of that resource
        """
        url = '%s/points-line/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'GET')
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def CreatePointsLine(self, data):
        url = '%s/points-line/' % (self.base_url)
        resp = self._RequestUrl(url, 'POST', data)
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def UpdatePointsLine(self, resid, data):
        """ Updates record data

        Args:
            resid(int, str):
                The id we want to update.
            data(dict):
                The data to update

        Returns:

        """
        url = '%s/points-line/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'PATCH', data)
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def DeletePointsLine(self, resid):
        """Deletes the resource

        Args:
            resid(int, str):
                The id we want to delete.

        Returns:
            True if successfull
        """
        url = '%s/points-line/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'DELETE')
        self._RaiseForHeaderStatus(resp)
        return True

    def GetMilesLine(self, resid):
        """Returns a single resource.

        Args:
            resid(int, str):
                The id we want to retrieve.

        Returns:
          A JSON representation of that resource
        """
        url = '%s/miles-line/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'GET')
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def CreateMilesLine(self, data):
        url = '%s/miles-line/' % (self.base_url)
        resp = self._RequestUrl(url, 'POST', data)
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def UpdateMilesLine(self, resid, data):
        """ Updates record data

        Args:
            resid(int, str):
                The id we want to update.
            data(dict):
                The data to update

        Returns:

        """
        url = '%s/miles-line/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'PATCH', data)
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def DeleteMilesLine(self, resid):
        """Deletes the resource

        Args:
            resid(int, str):
                The id we want to delete.

        Returns:
            True if successfull
        """
        url = '%s/miles-line/%s' % (self.base_url, resid)
        resp = self._RequestUrl(url, 'DELETE')
        self._RaiseForHeaderStatus(resp)
        return True

    def UpdateStockQuant(self, data):
        """ Updates record data

        Args:
            resid(int, str):
                The id we want to update.
            data(dict):
                The data to update

        Returns:

        """
        url = '%s/quants/' % (self.base_url, )
        resp = self._RequestUrl(url, 'PATCH', data)
        self._RaiseForHeaderStatus(resp)
        data = self._ParseAndCheck(resp)

        return data

    def _RaiseForHeaderStatus(self, response):
        """Raises an exception if an HTTP error ocurred

        Raises:
            (ayurdevas.AyurdevasError): AyurdevasError wrapping the error
            message
        """
        # # If status code is `unprocessable` do not raise exception
        # # as the response body needs to be processed for error details
        # if response.status_code != requests.codes.unprocessable_entity:
        if response.status_code == 422:
            self._ParseAndCheck(response)
            raise AyurdevasError(str(e))

        response.raise_for_status()

    def _RequestUrl(self, url, verb, data=None):
        """Request a url.

        Args:
            url:
                The web location we want to retrieve.
            verb:
                Either POST or GET.
            data:
                A dict of (str, unicode) key/value pairs.

        Raises:
            (ayurdevas.AyurdevasError): AyurdevasError wrapping the error
            message

        Returns:
            A requests.response object.
        """
        if not data:
            data = {}

        resp = 0

        try:
            if verb == 'GET':
                resp = requests.get(url, params=data, headers=self._request_headers)
            elif verb in ('PATCH', 'PUT'):
                resp = requests.patch(url, json=data, headers=self._request_headers)
            elif verb == 'POST':
                resp = requests.post(url, json=data, headers=self._request_headers)
            elif verb == 'DELETE':
                resp = requests.delete(url, headers=self._request_headers)
            else:
                raise AyurdevasError('Unknown REST Verb: {0}'.format(verb))
        except requests.exceptions.RequestException as e:
            raise e

        return resp

    def _ParseAndCheck(self, response):
        """Try and parse the JSON returned and return
        an empty dictionary if there is any error.

        This is a purely defensive check because during some
        network outages it will return an HTML fail page.

        Args:
            response (requests.models <Response> class):
                A response object created from the response content

        Raises:
            (ayurdevas.AyurdevasError): AyurdevasError wrapping the error
            message
        """
        try:
            data = response.json()
        except ValueError as e:
            raise AyurdevasError('JSON parse error: {0}'.format(str(e)))
        self._CheckForError(data)
        return data.get('data', {})

    def _CheckForError(self, data):
        """Raises a AyurdevasError if data has an error message.

        Args:
            data (dict):
                A python dict created from the json response

        Raises:
            (ayurdevas.AyurdevasError): AyurdevasError wrapping the error
            message if one exists.
        """
        # Errors are relatively unlikely, so it is faster
        # to check first, rather than try and catch the exception
        if 'error' in data:
            raise AyurdevasError(data['error'])

    def SetUserAgent(self, user_agent):
        """Override the default user agent.

        Args:
          user_agent:
            A string that should be send to the server as the user-agent.
        """
        self._request_headers['User-Agent'] = user_agent

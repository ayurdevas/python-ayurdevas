#!/usr/bin/env python


class AyurdevasError(Exception):
    """Base class for Ayurdevas errors"""

    @property
    def message(self):
        '''Returns the first argument used to construct this error.'''
        return self.args[0]

